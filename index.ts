import { Rcon } from "rcon-client/lib";
import { setInterval } from "timers";
import { processDonation } from "./actions";
import { tiltifyRconConfiguration } from "./config";
import { TiltifyApi } from "./tiltify";

const tiltifyApi = new TiltifyApi(tiltifyRconConfiguration.tiltifyToken);

let rconTimeout: NodeJS.Timeout;
let donationTimeout: NodeJS.Timeout;

function rconConnected(rconClient: Rcon)
{
    console.log("RCON client connected.");
    if (rconTimeout)
    {
        clearTimeout(rconTimeout);
    }
    donationTimeout = tiltifyApi.getDonationStream(
        tiltifyRconConfiguration.tiltifyCampaignId,
        tiltifyRconConfiguration.tiltifyPollInterval,
        d => processDonation(d, rconClient),
        tiltifyRconConfiguration.tiltifyEmitPreviousDonation)
}

function rconDisconnected()
{
    console.log("RCON client disconnected.");
    if (donationTimeout)
    {
        clearTimeout(donationTimeout);
    }
    rconTimeout = startRcon();
}

function startRcon(): NodeJS.Timeout
{
    let attemptInProgress: boolean = false;

    return setInterval(async () => {
            if (attemptInProgress)
            {
                return;
            }
        
            attemptInProgress = true;
            console.log("Attempting to connect to RCON server...");
            const rconClient = new Rcon({
                host: tiltifyRconConfiguration.rconHost,
                port: tiltifyRconConfiguration.rconPort,
                password: tiltifyRconConfiguration.rconPassword,
            });
            rconClient.on("connect", () => rconConnected(rconClient));
            rconClient.on("error", (reason) => {
                console.log(`RCON connection failed. Reason: ${reason instanceof Error ? reason.message : reason}`);
                rconDisconnected();
            });
            rconClient.on("end", () => rconDisconnected());

            try
            {
                await rconClient.connect();
            }
            catch (err)
            {
                console.log(`RCON connection failed. Reason: ${err.message}`);
                if (err.errno === 'ECONNREFUSED')
                {
                    console.log("Check that your Minecraft server is running and RCON is enabled with a password.");
                }
            }
            attemptInProgress = false;
        },
        tiltifyRconConfiguration.rconConnectInterval);
}

rconTimeout = startRcon();
