import { Configuration } from './config-types';

export const tiltifyRconConfiguration: Configuration = {
    // RCON host
    rconHost: 'localhost',
    // RCON port
    rconPort: 25575,
    // RCON password
    rconPassword: 'password1234',
    // Wait time in between RCON connect/re-connect attempts, in ms
    rconConnectInterval: 5000,

    // Tiltify OAuth token (see README.md for how to get one)
    tiltifyToken: '10b56ef89e09702d4dda5860d9e5c37db0af44bcb7cb49d869be94438ad58294',
    // Interval at which to poll for new donations, in ms
    tiltifyPollInterval: 5000,
    // Tiltify campaign ID to watch for donations
    tiltifyCampaignId: 8008,
    // Whether to run an action for the most recent pre-existing donation when the bot first starts up.
    // Enabling this allows you to test the bot without making repeated donations.
    // Ensure this is set to false in production.
    tiltifyEmitPreviousDonation: false,

    // Mapping donation amounts to sets of MC commands to execute via rcon.
    // If there is an action with exactAmount set to true and minDonation matching the donation amount, it will be used.
    // Otherwise, the action with the largest minDonation less than or equal to the actual donation amount (and
    // exactAmount set to false) will apply.
    // In this example configuration, a donation of $10.00 will trigger the $10.00 action, but a donation  of $10.50
    // will trigger the action for $5.00, because the $10.00 action has exactAmount set to true.
    actions: [
        {
            minDonation: 5.00,
            exactAmount: false,
            rconCommands: ['say Someone donated at least $5.00!']
        },
        {
            minDonation: 10.00,
            exactAmount: true,
            rconCommands: ['say Someone donated exactly $10.00!']
        },
        {
            minDonation: 1000.00,
            exactAmount: false,
            rconCommands: [
                'say Someone donated at least $1,000.00!!!',
                'say Adrien Brody'
            ]
        }
    ]
}
