export interface Configuration {
    rconHost: string;
    rconPort: number;
    rconPassword: string;
    rconConnectInterval: number;

    tiltifyToken: string;
    tiltifyPollInterval: number;
    tiltifyCampaignId: number;
    tiltifyEmitPreviousDonation: boolean;

    actions: ActionConfiguration[];
}

export interface ActionConfiguration {
    minDonation: number;
    exactAmount: boolean;
    rconCommands: string[];
}
