# Tiltify RCON Bot
Made with <3 by 0x4252414e444f4e

FOR THE KIDS!

# Steps
1. Make sure [node](https://nodejs.org/) is installed.
    - This project was developed and tested against node v12.18.3. YMMV with earlier versions.
2. Clone or download+extract this repository.
3. From the directory where you cloned or extraced the project, run `npm install` to retrieve dependencies.
4. Modify the `config.ts` file as appropriate.
    - You will have to rebuild the project on changing the configuration (see the next step).
    - See [Obtaining a Tiltify API Key](#obtaining-a-tilitfy-api-key) for information on that part of the configuration.
5. From the directory where you cloned or extracted the project, run `npm run-script build`.
    - On Windows, run `npm run-script build-win` instead.
6. Run `node build/index.js` to start the bot.
    - If nothing happens for several seconds, the bot is probably working! If the RCON or Tiltify configuration is wrong, you'll see errors quickly, even if no donations happen.

# Obtaining a Tiltify API Key
1. Go to https://tiltify.com/@me/dashboard/account/apps. Sign in or create an account (you do not have to use the same account that owns the campaign).
2. Click "New application".
3. Enter any name and redirect URL for the application.
    - The name may be anything. `0xTiltifyRcon`, for example.
    - The redirect URL is used for OAuth, which this bot does not use. Simply provide any valid URL, such as `https://example.com/`. The URL must use `https`.
4. Click "create". The application appears in the table at the bottom of the screen.
5. Click "view credentials", then copy and paste the Access token into `config.ts`.